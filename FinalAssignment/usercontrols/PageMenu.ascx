﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageMenu.ascx.cs" Inherits="FinalAssignment.usercontrols.PageMenu" %>
<asp:SqlDataSource 
    runat="server" 
    ID="pages_select"
    ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
</asp:SqlDataSource>
<ul class="nav navbar-nav" id="menucontainer" runat="server">
    <li><a runat="server" href="~/pageManagement.aspx">Manage</a></li>
</ul>