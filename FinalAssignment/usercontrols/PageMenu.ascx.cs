﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalAssignment.usercontrols
{
    public partial class PageMenu : System.Web.UI.UserControl
    {

        private string basequery = "SELECT page_id as id, page_title as title from user_created_pages ";

        
        protected void Page_Load(object sender, EventArgs e)
        {

            pages_select.SelectCommand = basequery;

            DataView myview = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);
            string menustring = "";
            foreach (DataRowView myrow in myview)
            {
                string pageid = myrow["id"].ToString();
                string page_title = myrow["title"].ToString();
                menustring += "<li><a href=\"viewPage.aspx?page_id="+pageid+"\">"+page_title+"</a></li>";
            }
            menucontainer.InnerHtml = menustring;
        }


    }
}