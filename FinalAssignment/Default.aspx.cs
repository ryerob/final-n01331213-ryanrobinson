﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalAssignment
{
    public partial class _Default : Page
    {
        protected void toManagePages(object sender, EventArgs e)
        {
            Response.Redirect("/pageManagement.aspx");
        }
    }
}