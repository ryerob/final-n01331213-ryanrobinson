﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="updatePage.aspx.cs" Inherits="FinalAssignment.editPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>
    <div class="pageForm" runat="server">
        <h1>Update Page</h1>
        <p id="message" runat="server"></p>
        <div class="titleInput">
            <label>Title: </label>
            <asp:TextBox ID="page_title" type="text" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title" ErrorMessage="Enter a title for your page."></asp:RequiredFieldValidator>
        </div>
        <div class="authorInput">
            <label>Author: </label>
            <asp:TextBox ID="author_name" type="text" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="author_name" ErrorMessage="Enter an author for this post."></asp:RequiredFieldValidator>
        </div>
        <div class="contentInput">
            <label>Content: </label>
            <asp:TextBox ID="page_content" type="text" CssClass="contentBox" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="page_content" ErrorMessage="Enter a title for your page."></asp:RequiredFieldValidator>
        </div>
    </div>
    <ASP:Button Text="Edit Page" runat="server" class="myButton" OnClick="Update_Page"/>
    <div id="debug" runat="server">

    </div>
</asp:Content>