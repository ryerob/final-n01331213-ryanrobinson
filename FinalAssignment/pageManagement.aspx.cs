﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalAssignment
{
    public partial class pageManagement : System.Web.UI.Page
    {

        private string basequery = "SELECT page_id, page_title as 'Page Title', author_name as 'Author Name', date_published as 'Date'" +
           "FROM user_created_pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_select.SelectCommand = basequery;
            pages_data.DataSource = Classes_Manual_Bind(page_select);
            pages_query.InnerHtml = basequery;
            pages_data.DataBind();
        }
        protected DataView Classes_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["Page Title"] =
                    "<a href=\"viewPage.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + row["Page Title"]
                    + "</a>";
            }
            mytbl.Columns.Remove("page_id");
            myview = mytbl.DefaultView;

            return myview;
        }
        protected void toCreate(object sender, EventArgs e)
        {
            Response.Redirect("/createPage.aspx");
        }
        }
}