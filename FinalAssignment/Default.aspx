﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FinalAssignment._Default" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Create Web Pages</h1>
        <p class="lead">Create, read, update and delete web pages.</p>
        <asp:Button runat="server" OnClick="toManagePages" class="myButton" Text="Manage Pages" />
    </div>
</asp:Content>
