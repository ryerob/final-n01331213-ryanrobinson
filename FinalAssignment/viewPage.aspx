﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="viewPage.aspx.cs" Inherits="FinalAssignment.viewPage" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 id="pageTitle" runat="server"></h1>
    
    <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>

    <asp:DataGrid ID="page_list" runat="server">

    </asp:DataGrid>
    <div id="debug" runat="server"></div>
    <p>Author: <span id="pageAuthorPrint" runat="server"></span></p>
    <p>Date: <span id="pageDatePrint" runat="server"></span></p>
    
    <div id="pageContentPrint" runat="server">

    </div>
    <asp:Button runat="server" OnClick="EditPage" class="myButton" Text="Edit Page" />
    <asp:Button runat="server" OnClick="DeletePage"
        OnClientClick="if(!confirm('This page will be permanently deleted.')) return false;"
        class="myButton" Text="Delete Page" />
</asp:Content>