﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalAssignment
{
    public partial class viewPage : System.Web.UI.Page
    {

        private string basequery = "SELECT page_title as 'Title', author_name as 'Author', page_content as 'Content', date_published as 'Date' FROM user_created_pages";

        private string pageid
        {
            get { return Request.QueryString["page_id"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            basequery += " WHERE page_id = " + pageid;
            debug.InnerHtml = basequery;
            
            page_select.SelectCommand = basequery;

            DataView pageView = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            string pageNameHeading = pageView[0]["Title"].ToString();
            pageTitle.InnerHtml = pageNameHeading;

            string pageAuthor = pageView[0]["Author"].ToString();
            pageAuthorPrint.InnerHtml = pageAuthor;

            string pageContent = pageView[0]["Content"].ToString();
            pageContentPrint.InnerHtml = pageContent;

            string pageDate = pageView[0]["Date"].ToString();
            pageDatePrint.InnerHtml = pageDate;
        }

        protected void DeletePage(object sender, EventArgs e)
        {
            string deletequery = "DELETE FROM user_created_pages WHERE page_id=" + pageid;

            page_select.DeleteCommand = deletequery;
            page_select.Delete();

            Response.Redirect("/pageManagement.aspx");
        }

        protected void EditPage(object sender, EventArgs e)
        {
            Response.Redirect("/updatePage.aspx?page_id="+pageid);
        }
    }
}
