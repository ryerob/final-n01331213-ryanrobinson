﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="pageManagement.aspx.cs" Inherits="FinalAssignment.pageManagement" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<h1>Page Management</h1>

<asp:SqlDataSource runat="server"
    id="page_select"
    ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
</asp:SqlDataSource>

<asp:DataGrid id="pages_data" 
    runat="server" >
</asp:DataGrid>
<br />
<asp:Button runat="server" OnClick="toCreate" Text="Create Page" class="myButton" />

<div id="pages_query" class="querybox" runat="server">

</div>
</asp:Content>
