﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalAssignment
{
    public partial class addNewPage : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO user_created_pages" +
            "(page_title, author_name, page_content) VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CreatePage(object sender, EventArgs e)
        {
            string title = page_title.Text.ToString();
            string author = author_name.Text.ToString();
            string content = page_content.Text.ToString();

            addquery += "('" + title + "', '" + author + "', '" +
            content + "')";


            page_select.InsertCommand = addquery;
            page_select.Insert();

            Response.Redirect("/pageManagement.aspx");
        }
    }
}