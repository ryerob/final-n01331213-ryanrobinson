﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalAssignment
{
    public partial class editPage : System.Web.UI.Page
    {
        public int page_id
        {
            get { return Convert.ToInt32(Request.QueryString["page_id"]); }
        }

        private string updatequery = "SELECT page_title, author_name, page_content from user_created_pages";
        protected void Page_Load(object sender, EventArgs e)
        {
           // page_title.Text = 
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (page_id.ToString() == null)
            {
                message.InnerHtml = "No Page Found.";
                return;
            }
            else
            {
                page_select.SelectCommand = updatequery + " WHERE page_id=" + page_id;

                DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
                if (pageview.ToTable().Rows.Count == 0) message.InnerHtml = "No page found.";
                else
                {
                    DataRowView pagerowview = pageview[0];
                    var title = pagerowview["page_title"].ToString();
                    var author = pagerowview["author_name"].ToString();
                    var content = pagerowview["page_content"].ToString();

                    page_title.Text = title;
                    author_name.Text = author;
                    page_content.Text = content;
                }
            }
        }

        protected void Update_Page(object sender, EventArgs e)
        {
            string title = page_title.Text.ToString();
            string author = author_name.Text.ToString();
            string content = page_content.Text.ToString();

            string updateQuery = "Update user_created_pages set page_title='" + title + "'," +
                " author_name='" + author + "'," +
                " page_content='" + content + "'" +
                " where page_id=" + page_id;
            debug.InnerHtml = updateQuery;

            page_select.UpdateCommand = updateQuery;
            page_select.Update();

            Response.Redirect("/pageManagement.aspx");
        }
    }
}