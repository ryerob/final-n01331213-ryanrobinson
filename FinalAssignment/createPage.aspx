﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="createPage.aspx.cs" Inherits="FinalAssignment.addNewPage" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>

    <div class="pageForm" runat="server">
        <h1>Add New Page</h1>
        <div class="titleInput">
            <label>Title: </label>
            <asp:TextBox id="page_title" type="text" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title" ErrorMessage="Enter a title for your page."></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="authorInput">
            <label>Author: </label>
            <asp:TextBox id="author_name" type="text" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="author_name" ErrorMessage="Enter the name of the author"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="contentInput">
            <label>Content: </label>
            <asp:TextBox id="page_content" type="text" runat="server" textmode="multiline" CssClass="contentBox"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="page_content" ErrorMessage="Enter some content for your page."></asp:RequiredFieldValidator>
        </div>
        <br />
    </div>
    <ASP:Button Text="Create New Page" class="myButton" runat="server" OnClick="CreatePage"/>
</asp:Content>
